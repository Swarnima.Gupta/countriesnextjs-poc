import "bootstrap/dist/css/bootstrap.min.css"
import styles from "./CountryTable.module.css"
import Link from "next/link";

const CountriesTable = ({countries}) =>{
    console.log(countries)
    return <div>
        {countries.map((country) => (
            <div className={styles.card}>
               <div className={styles.cardBody}>
                   <div className="row">
                       <div className="col-md-6">
                           <img src={country.flag}
                                className="img-fluid"/>
                       </div>

                       <div className="col-md-6">
                             < div className={styles.details}>
                              <div className={styles.countryhead}>{country.name}</div>
                                 <div className={styles.countrycurrency}>
                                     {country.currencies && country.currencies.map((code, index) =>
                                     <dd key={code.id}>
                                     Currency : {' ' }<span>{code.name}</span>
                                 </dd>)}</div>
                                 <div className={styles.countrybutton}>
                                    <div className={styles.linkbutton}>
                                        <Link href={"https://www.google.com/maps/?q=" + country.name}>
                                            <button className="btn btn-outline-primary ">Show Map</button>
                                        </Link>
                                    </div>

                                    <div className={styles.linkbutton} >
                                        <Link href={`/country/${country.alpha3Code}`}>
                                            <button className="btn btn-outline-primary">
                                                Show Details
                                            </button>
                                        </Link>
                                    </div>
                                 </div>

                             </div>
                       </div>
                   </div>
               </div>
            </div>
        ))}
    </div>

}

export default CountriesTable;