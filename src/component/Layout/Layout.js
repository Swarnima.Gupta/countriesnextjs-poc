import styles from "../../styles/Home.module.css";
import Head from "next/head";

const Layout = ({children,title="Countries"})  => {
    return (
        <div className="container">
            <Head>
                <title>{title}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className={styles.main}>
                {children}
            </main>


        </div>
    )
}

export default Layout;