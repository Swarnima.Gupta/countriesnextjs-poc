import Layout from "../../component/Layout/Layout";
import "bootstrap/dist/css/bootstrap.min.css"
import styles from "./country.module.css"
import Link from "next/link";
import {getCountry} from "../index";
import {useState} from "react";



const Country = ({country}) => {
     console.log(country);
    return <Layout title={country.name}>
        <div>
            <span className={styles.countrymain}>{country.name}</span>
        </div>
      <div className="row">
       <div className="col-md-6">


           <div>
               <img src={country.flag} className="img-fluid"/>
           </div>
       </div>

          <div className="col-md-6">
             <div className={styles.countrydetails}>
                 <div>Native Name:
                     {country.translations.fa}
                 </div>
                 <div>Capital:{country.capital}</div>
                 <div>Population:{country.population}</div>
                 <div>Area:{country.area} km2</div>
                 <div>Country Code:+{country.callingCodes}</div>
                 <div>Region:{country.region}</div>
                 <div>Sub Region:{country.subregion}</div>
                 <div>{country.languages && country.languages.map((language,index) =>
                     <div key={language.id}>
                       Languages:  {language.name}
                     </div>
                 )}</div>

             </div>
          </div>
      </div>
        <h3 className={styles.countrymain}>Neighbours</h3>
        <div className="row">
            <div className="col-md-6">
                <div className={styles.countrydetails}>
                    {country.borders && country.borders.map((border,index) =>
                        <div key={border.id}>
                            {border}

                        </div>
                    )}
                </div>

            </div>
        </div>
    </Layout>
}

export default Country;


export const getServerSideProps = async ({params}) =>{
    const country = await getCountry(params.id);
    return{
        props:{
            country,
        },
    };
};