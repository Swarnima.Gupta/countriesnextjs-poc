import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from "../component/Layout/Layout";
import SearchInput from "../component/SearchInput/SearchInput";
import CountriesTable from "../component/CountriesTable/CountryTable";
import {useState} from "react";

export default function Home({countries}) {
       const [keyword ,setKeyword] = useState("");

    const filteredCountries = countries.filter(country =>
    country.name.toLowerCase().includes(keyword));

    const onInputChange = (e) =>{
        e.preventDefault();
        setKeyword(e.target.value.toLowerCase());
    }


  return <Layout>
      <div className={styles.counts}>


          <SearchInput placeholder="Search for Countries"
           onChange={onInputChange}
          />
           <CountriesTable countries={filteredCountries} />
      </div>
  </Layout>


}


export const getStaticProps = async  () =>{
    const res = await fetch("https://restcountries.eu/rest/v2/all");
    const countries = await res.json();

    return{
        props:{
            countries,
        },
    };
};



export const getCountry = async (id) =>{
    const res = await fetch(
        `https://restcountries.eu/rest/v2/alpha/${id}`);
    const country = await res.json();
    return country;
}